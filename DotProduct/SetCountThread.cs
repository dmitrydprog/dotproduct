﻿using System;
using System.Windows.Forms;

namespace DotProduct
{
	public partial class SetCountThread : Form
	{
		#region Public Constructors

		public SetCountThread()
		{
			InitializeComponent();
		}

		#endregion Public Constructors

		#region Public Properties

		public int CountThread
		{
			get
			{
				return (int)numericUpDown1.Value;
			}
		}

		#endregion Public Properties

		#region Private Methods

		private void button1_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}

		#endregion Private Methods
	}
}