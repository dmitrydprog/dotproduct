﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DotProduct
{
	public class ThreadEntityFactory : IDisposable
	{
		#region Private Fields

		private List<ThreadEntity> _threadList;

		private int endWorkCounter;

		private object lockObject = new object();

		#endregion Private Fields

		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр фабрике потоков.
		/// </summary>
		/// <param name="data">Набор входных данных.</param>
		/// <param name="countThread">Кол-во потоков.</param>
		public ThreadEntityFactory(IEnumerable<Tuple<double, double>> data, int countThread)
		{
			this.Data = data;
			this.CountThread = countThread;

			if (data.Count() <= countThread)
				this.CountThread = this.Data.Count();

			_threadList = new List<ThreadEntity>(countThread);
			_threadList.AddRange(Enumerable.Range(0, this.CountThread).Select(x => new ThreadEntity()));

			foreach( var thread in _threadList )
				thread.OnEntityEndWork += ThreadEntityFactory_OnEntityEndWork;

			int overIter = 0;
			foreach (var i in data)
			{
				_threadList[overIter].Stack.Push(i);
				if (++overIter >= countThread) overIter = 0;
			}
		}

		public ThreadEntityFactory(IEnumerable<Tuple<double, double>> data)
			: this(data, Environment.ProcessorCount)
		{ }

		#endregion Public Constructors

		#region Public Events
		/// <summary>
		/// Событие которое вызывается когда все потоки завершат свое выполнения.
		/// </summary>
		public event EventHandler<EventArgs> OnComplete;

		#endregion Public Events

		#region Public Properties
		/// <summary>
		/// Форма для вывода.
		/// </summary>
		public MainForm Form { get; set; }
		/// <summary>
		/// Кол-во потоков.
		/// </summary>
		public int CountThread { get; set; }
		/// <summary>
		/// Набор входных данных.
		/// </summary>
		public IEnumerable<Tuple<double, double>> Data { get; set; }
		/// <summary>
		/// Глобальная сумма.
		/// </summary>
		public double GlobalSum { get; set; }
		/// <summary>
		/// Глобальное время выполнения.
		/// </summary>
		public TimeSpan GlobalTime { get; set; }

		#endregion Public Properties

		#region Public Methods

		/// <summary>
		/// Стартует все потоки.
		/// </summary>
		public void Start()
		{
			_threadList.ForEach(x => x.Thread.Start());
		}

		#endregion Public Methods

		#region Private Methods

		private void ThreadEntityFactory_OnEntityEndWork(object sender, EntityEndWorkArgs e)
		{
			lock (this.lockObject)
			{
				this.GlobalSum += e.Sum;
				this.GlobalTime += e.WorkTime;
				this.endWorkCounter++;

				var text = $"Поток {(sender as ThreadEntity).Thread.Name} завершил свое выполнения.\n" +
						   $"Время выполнения потока: {e.WorkTime.ToString()}\n" +
						   $"Частичная сумма потока: {e.Sum}\n" +
						   "---------------------------------------------------------\n";

				Statistic.PrintInfo(this.Form, text);

                if (this.endWorkCounter == this.CountThread)
					if (this.OnComplete != null)
						this.OnComplete(this, new EventArgs());
			}
		}

		public void Dispose()
		{
			for (int i = 0; i < this._threadList.Count; i++)
				this._threadList[i] = null;
		}

		#endregion Private Methods
	}
}