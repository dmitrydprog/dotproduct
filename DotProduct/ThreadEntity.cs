﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace DotProduct
{
	/// <summary>
	/// Аргументы для события о завершение вычислений.
	/// </summary>
	public class EntityEndWorkArgs
	{
		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр текущего класса.
		/// </summary>
		/// <param name="sum">Сумма которая получилась в итоге при умножение соответствующих координат векторов.</param>
		/// <param name="workTime">Время работы потока.</param>
		public EntityEndWorkArgs(double sum, TimeSpan workTime)
		{
			this.Sum = sum;
			this.WorkTime = workTime;
		}

		#endregion Public Constructors

		#region Public Properties

		/// <summary>
		/// Сумма умножений координат векторов.
		/// </summary>
		public double Sum { get; private set; }

		/// <summary>
		/// Время работы потока.
		/// </summary>
		public TimeSpan WorkTime { get; private set; }

		#endregion Public Properties
	}

	/// <summary>
	/// Класс представляющий из себя локальную сущность для вычисления определенного набора данных.
	/// </summary>
	public class ThreadEntity
	{
		private static int id = 0;

		/// <summary>
		/// Очистка id потоков.
		/// </summary>
		public static void Clear()
		{
			id = 0;
		}

		/*
		* В случае с текущей задачей данные будут представлять из себя пары координат вектора (X1..Xn; Y1..Yn).
		*/
		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр текущего класса.
		/// </summary>
		/// <param name="func">Функция которую нужно применять к каждому значению из набора данных.</param>
		public ThreadEntity()
		{
			/*
			* В нашем случае набором данных является набор пар координат векторов.
			* Например [ (1, 1), (2, 2), (-3, 7) .. ].
			* Функцией которую мы должны будем применять на каждую пару координат (x, y) будет:
			* f((x, y)) = x * y. Простое умножение координат.
			* Но так как потоки получают на вход не одну пару а сразу набор таких пар,
			* то нам необходима результирующая функция которая будет суммировать умноженные пары
			* исходя из формулы скалярного произведения: 
			* (x1, x2, ..., xn) * (y1, y2, ..., yn) = x1 * y1 + y2 * y2 + ... + xn * yn.
			* В нашем случае данной функцией будет функция "work".
			*/
			this.Stack = new Stack<Tuple<double, double>>();
			WorkResult = 0;

			this.Thread = new Thread(work) { Name = id.ToString() };
			id++;
		}

		/// <summary>
		/// Функция для вычисления суммы промежутка.
		/// </summary>
		private void work()
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			while(this.Stack.Count != 0 )
			{
				var tmp = this.Stack.Pop();
				this.WorkResult += tmp.Item1 * tmp.Item2;
			}

			stopwatch.Stop();

			if (OnEntityEndWork != null)
				this.OnEntityEndWork(this, new EntityEndWorkArgs(WorkResult, stopwatch.Elapsed));
		}

		#endregion Public Constructors

		#region Public Events
		/// <summary>
		/// Событие которое возникает после завершения поток всех действий над данными.
		/// </summary>
		public event EventHandler<EntityEndWorkArgs> OnEntityEndWork;

		#endregion Public Events

		#region Public Properties

		/// <summary>
		/// Набор входящих данных который будем обрабатывать.
		/// </summary>
		public Stack<Tuple<double, double>> Stack { get; set; }
		/// <summary>
		/// Ссылка на экземпляр потока который будем использовать.
		/// </summary>
		public Thread Thread { get; set; }
		/// <summary>
		/// Результат скалярного умножения отрезко для этого потока.
		/// </summary>
		public double WorkResult { get; set; }

		#endregion Public Properties
	}
}