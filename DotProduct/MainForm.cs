﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace DotProduct
{
	public partial class MainForm : Form
	{
		#region Private Fields

		private Random rand;

		#endregion Private Fields

		#region Public Constructors

		public MainForm()
		{
			InitializeComponent();

			Statistic.RecomendCountThread = Environment.ProcessorCount;
			Statistic.UpdateForm(this);

			rand = new Random();
			numericUpDown1_ValueChanged(this, new EventArgs());
		}

		#endregion Public Constructors

		#region Public Methods

		public T GetComponentByName<T>(string name) where T : Control
		{
			return this.RecOfType<T>().FirstOrDefault(x => x.Name == name);
		}

		#endregion Public Methods

		#region Private Methods

		private void button1_Click(object sender, EventArgs e)
		{
			numericUpDown1_ValueChanged(this, new EventArgs());
		}

		private void button2_Click(object sender, EventArgs e)
		{
			listBox1.Items.Clear();
			ThreadEntity.Clear();

			var list = new LinkedList<Tuple<double, double>>();
			var rand = new Random();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int i = 0; i < Statistic.CountValueInVector; i++)
				list.AddLast(new Tuple<double, double>(rand.NextDouble(), rand.NextDouble()));

			stopwatch.Stop();

			var text = $"Создание тестового набора данных завершилось.\n" +
					   $"Время создания: {stopwatch.Elapsed.ToString()}\n";

			Statistic.PrintInfo(this, text);

			stopwatch.Start();
			using (var factory = new ThreadEntityFactory(list, Statistic.RecomendCountThread) { Form = this })
			{
				text = $"Распределение данных по потокам завершилось.\n" +
					   $"Время распределения: {stopwatch.Elapsed.ToString()}\n";

				Statistic.PrintInfo(this, text);
				stopwatch.Stop();

				factory.OnComplete += Factory_OnComplete;
				factory.Start();
			}
		}

		private void Factory_OnComplete(object sender, EventArgs e)
		{
			var factory = sender as ThreadEntityFactory;
			var text = $"Скалярное произведения = {factory.GlobalSum}\n" +
					   $"Общее время выполнения = {factory.GlobalTime.ToString()}";
			
			Statistic.PrintInfo(this, text);
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			Statistic.CountValueInVector = (int)numericUpDown1.Value;
		}

		private void setCountThread_Click(object sender, EventArgs e)
		{
			SetCountThread sct = new SetCountThread();

			if (sct.ShowDialog() == DialogResult.OK)
			{
				Statistic.RecomendCountThread = sct.CountThread;
				Statistic.UpdateForm(this);
			}
		}

		#endregion Private Methods
	}
}