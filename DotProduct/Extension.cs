﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace DotProduct
{
	/// <summary>
	/// Класс расширений необходимый для получения protected контролов с формы по имени.
	/// </summary>
	public static class Extension
	{
		#region Public Methods

		/// <summary>
		/// Получает список контролов по их типу.
		/// </summary>
		/// <typeparam name="T">Тип нужных контролов.</typeparam>
		/// <param name="collection">Коллекция контролов с которых нужно выбрать необходимые.</param>
		/// <returns>Список подходящих контролов.</returns>
		public static IEnumerable<T> OfType<T>(this Control.ControlCollection collection) where T : class
		{
			foreach (var control in collection)
			{
				T tmp = control as T;
				if (tmp != null) yield return tmp;
			}
		}

		/// <summary>
		/// Рекурсивно обходит все коллекции контролов и ворачивает контролы с подходящим типом.
		/// </summary>
		/// <typeparam name="T">Тип нужных контролов.</typeparam>
		/// <param name="control">Корень для рекурсивного обхода.</param>
		/// <returns>Список подходящих контролов.</returns>
		public static IEnumerable<T> RecOfType<T>(this Control control) where T : class
		{
			foreach (var ctrl in control.Controls.OfType<T>())
				yield return ctrl;

			foreach (Control ctrl in control.Controls)
				foreach (var tmp in RecOfType<T>(ctrl))
					yield return tmp;
		}

		#endregion Public Methods
	}
}