﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DotProduct
{
	/// <summary>
	/// Статический класс содержаший необходимую статистику.
	/// </summary>
	public static class Statistic
	{
		#region Public Properties

		/// <summary>
		/// Кол-во потоков используемых для вычисления.
		/// </summary>
		public static int RecomendCountThread { get; set; }
		/// <summary>
		/// Размерность векторов.
		/// </summary>
		public static int CountValueInVector { get; set; }
		#endregion Public Properties

		#region Public Methods

		/// <summary>
		/// Выводит в поле статистики текст.
		/// </summary>
		/// <param name="form">Форма на которой находится поле.</param>
		/// <param name="text">Текст который нужно вывести.</param>
		public static void PrintInfo(MainForm form, string text)
		{
			var component = form.GetComponentByName<ListBox>("listBox1");

			if (component.InvokeRequired)
			{
				component.Invoke(new Action(() => component.Items.AddRange(text.Split('\n'))));
			}
			else
			{
				component.Items.AddRange(text.Split('\n'));
			}
		}

		/// <summary>
		/// Обновляет данные статистики на форме.
		/// </summary>
		/// <param name="form">Форма на которой нужно обновить данные.</param>
		public static void UpdateForm(MainForm form)
		{
			form.GetComponentByName<Label>("countThreadLabel").Text = RecomendCountThread.ToString();
		}

		#endregion Public Methods
	}
}